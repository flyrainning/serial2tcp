var config=require('./config');

var SerialPort = require('serialport');
var port = new SerialPort(config.device,config.options,function(){
  console.log("port open");
});


var net = require('net');
var client = new net.Socket();
client.connect(config.server.port,config.server.address, function() {
console.log("net open");
});



port.on('data', send_net);
client.on('data',send_port);


function send_port(data){
//  console.log(data);
  if (port.isOpen){

    port.write(data);
  }
}
function send_net(data){
//  console.log(data);
  if (!client.destroyed){

    client.write(data);
  }
}
