function h2i(h){
  return parseInt(h,16);
}
function i2h(i,n){
  return PrefixInteger(i.toString(16),n);
}
function PrefixInteger(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}



function getResultType(){
  return $("input[name='rtype']:checked").val();
}
function getSendType(){
  return $("input[name='stype']:checked").val();
}
function setbaudrate(){
  var b=$("#baudrate").val();
  b=i2h(parseInt(b),6);
  var cmd=new Uint8Array(8);
  cmd[0]=h2i("55");
  cmd[1]=h2i("AA");
  cmd[2]=h2i("55");

  cmd[3]=h2i(b.slice(0,2));
  cmd[4]=h2i(b.slice(2,4));
  cmd[5]=h2i(b.slice(4,6));

  cmd[6]=h2i("03");
  cmd[7]=cmd[3]+cmd[4]+cmd[5]+cmd[6];

  ws.send(cmd);
}
