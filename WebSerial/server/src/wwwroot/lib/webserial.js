var ws = new WebSocket("ws://"+window.location.hostname+":7003");

ws.onopen = function(){

  //ws.send("发送数据");

};

ws.onmessage = function (evt){
  var type=getResultType();
  if (type=="ascii"){
    readTXT(evt);
  }else{
    readHEX(evt);
  }
};

function readTXT(evt){
  var reader = new FileReader();
	reader.onload = function(e){
		if(e.target.readyState == FileReader.DONE){
      parseData(String(e.target.result),evt);
		}
	}
  reader.readAsText(evt.data);
}

function readHEX(evt){
  var reader = new FileReader();
		reader.onload = function(e){
			if(e.target.readyState == FileReader.DONE){
				parseHEX(new Uint8Array(e.target.result),evt);
			}
		}
		reader.readAsArrayBuffer(evt.data);
}
function parseHEX(data){
  var res="";
  for (var i = 0; i < data.length; i++) {
  	var d=i2h(data[i],2);
    res=res+d+" ";
  }
  res+="\n";
  parseData(res);
}
function parseData(data){
  $("#result").text($("#result").text()+data);
  toTextareaEnd('result');
}
function toTextareaEnd(id){
  document.getElementById(id).scrollTop=document.getElementById(id).scrollHeight;
}
