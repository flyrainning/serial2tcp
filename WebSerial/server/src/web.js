var config=require('./config');

var express = require('express');
var app = express();


//web server
app.use(express.static('wwwroot'));
var server = app.listen(config.port.web, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('listening at http://%s:%s', host, port);
});

module.exports=app;
