'use strict';


module.exports=
class tcp{
  constructor(){
    this.net = require('net');
    this.server = this.net.createServer();
    this.s=false;
    this.cb=function(data){};
    var that=this;

    this.server.on('connection', function(sock) {

        console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);
        that.s=sock;
        sock.on('data', function(data) {
          try{
            that.cb(data);
          }catch(e){}
        });
        sock.on('close', function() {
            that.s=false;
        });

    });


  }
  listen(port){
    this.server.listen(port);
    console.log('Server listening on ' + this.server.address().address + ':' + this.server.address().port);
  }
  send(data){
    try{
      if (this.s) this.s.write(data);
    }catch(e){}

  }
  ondata(callback){
    this.cb=callback;
  }
};
