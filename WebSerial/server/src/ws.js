var config=require('./config');

var cb=function(data){}
var w;

var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({ port: config.port.ws });

wss.on('connection', function connection(ws,req) {

  w=ws;
  ws.on('message', function(message,flags) {
  	try{
  		cb(message);
    }catch(e){}
  });
  ws.on('close', function() {
    w=false;
  });


});

module.exports={
  send:function(data){
    try{
      if (w) w.send(data);
    }catch(e){}

  },
  ondata:function(callback){
    cb=callback;
  }
}
