var config=require('./config');

var web = require('./web');
var ws = require('./ws');
var tcp = require('./tcp');
var client_in = new tcp();
var client_out = new tcp();

client_in.listen(config.port.in);
client_out.listen(config.port.out);

ws.ondata(function(data){
  console.log(data);
  client_in.send(data);
});

client_in.ondata(function(data){
  console.log(data);
  ws.send(data);
  client_out.send(data);
});
client_out.ondata(function(data){
  console.log(data);
  client_in.send(data);
});
