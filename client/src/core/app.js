"use strict";
var path = require('path');
var fs = require('fs');



class APP{
  constructor(_config="../config.js"){

    this.core_path=__dirname;
    this.config=require(path.join(this.core_path,_config));
    this.path=path;
    this.fs=fs;
    this.ROOT=path.join(this.core_path,"../");
    this.modules_path=path.join(this.ROOT,"modules");

  }
  FILE(name,name2){
    if (name2){
      name=this.path.join(name,name2)
    }

    if (this.path.isAbsolute(name)){
      return name;
    }else{
      return this.path.join(this.ROOT,name);
    }

  }
  modules(name){
    var pathname = this.path.join(this.modules_path,name);
    // console.log(pathname);
    return require(pathname);
  }
  load_local_script(path){
    this.run_script(this.load_file(path));
  }
  load_local_style(path){
    this.run_style(this.load_file(path));
  }
  load_script(url){
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url;
    document.head.appendChild(script);
  }
  load_style(url){
    var link = document.createElement('link');
    link.type='text/css';
    link.rel = 'stylesheet';
    link.href = url;
    document.head.appendChild(link);
  }
  run_script(code){
    var script = document.createElement("script");
    script.type = "text/javascript";
    try {
      script.appendChild(document.createTextNode(code));
    } catch (ex) {
      script.text(code);
    }
    document.head.appendChild(script);
  }
  run_style(code){
    var style = document.createElement("style");
    try {
      style.appendChild(document.createTextNode(code));
    } catch (ex) {
      style.text(code);
    }
    document.head.appendChild(style);
  }
  onload(callback){
    document.addEventListener( "DOMContentLoaded",callback);
  }
  ponload(callback){
    process.once( "onload",callback);
  }
  load_file(file){
    file=path.join(this.core_path,file);
    return fs.readFileSync(file);
  }

};

var app = new APP()
module.exports = app
