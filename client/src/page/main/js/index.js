
// const {app} = nodeRequire('electron').remote;


// console.error(1);
var serialPort = $app.modules('comm/serial');
var TcpPort = $app.modules('comm/TCPClient');
// console.error(2);
// var p=$app.FILE('modules/comm/TCPClient');
// console.log(3);
// console.log(p);
// var TcpPort = nodeRequire(p);
// console.log(TcpPort);

var serialCtrl = new Vue({
  el: '#SerialSettings',
  template:'#TempSerialSettings',
  data: {
    serial:"",
    baudrate:"115200",
    server:"47.105.113.220",
    port:"9999",
    msg:"",
    BaudratelList:["1200","4800","9600","19200","38400","57600","115200"],
    serialList:[],
    isrunning:false
  },
  methods: {

    getSerialList: function(){
      var that=this;
      that.serialList=[];
      serialPort.getPorts(function (err, ports) {
        ports.forEach(function(port) {
          that.serialList.push({
            name:port.comName
          });
          if (that.serial == "") that.serial = port.comName;
        });
      });
    },
    start:function(){
      var that=this;
      this.msg="";

      serialPort.ondata(function(data){
        console.log("s:",data);
        TcpPort.send(data);
      });
      TcpPort.ondata(function(data){
        console.log("t:",data);
        serialPort.send(data);
      });
      serialPort.onerror(function(msg){
        that.msg=msg;
        console.error(msg);
        that.stop();

      });
      TcpPort.onerror(function(msg){
        that.msg=msg;
        console.error(msg);
        that.stop();

      });
      serialPort.open(this.serial, this.baudrate);
      // serialPort.open("/dev/pts/7", this.baudrate);
      TcpPort.open(this.server, this.port);

      this.isrunning=true;

    },
    stop:function(){
      if (serialPort.isok) serialPort.stop();
      if (TcpPort.isok) TcpPort.stop();
      this.isrunning=false;
    }
  },
  mounted: function() {
    var that=this;
    this.$nextTick(function () {
      that.getSerialList();
    });
  },
  updated: function() {
    setTimeout(function(){
      mdui.Select("#SerialSettingsSerialName").handleUpdate();
      mdui.Select("#SerialSettingsBaudrate").handleUpdate();

    },10);

  }
});


function checkStatus(){

}

setInterval(checkStatus,500);
