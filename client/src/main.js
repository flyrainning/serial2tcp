// Modules to control application life and create native browser window
const {app, Menu,BrowserWindow,globalShortcut} = require('electron')

var $app = require('./core/app')
app.$app = $app

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow


//保证单实例运行
if (app.makeSingleInstance((commandLine, workingDirectory)=>{
  if (mainWindow) {
    if (mainWindow.isMinimized()) mainWindow.restore();
    mainWindow.focus();
  }
})){
  app.quit();
}

const path = require('path');
const url = require('url');



const config=$app.config;

// app.tcp=require("./modules/comm/TCPClient");


function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    title:" ",
    titleBarStyle:"hiddenInset",
    backgroundColor: '#FFF',
    center:true,
    resizable:config.debug || false,
    fullscreenable:false,
    width: config.size.width || 1000,
    height: config.size.height || 701,
    webPreferences:{
      preload:__dirname+"/core/core.js",
      webSecurity:false,
      allowRunningInsecureContent:true
    },
    show: false
  });

  app.mainWindow=mainWindow;

  var load_options={
    userAgent:"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36"
  }




  // const menu = Menu()
  Menu.setApplicationMenu(null);



  // mainWindow.setMenu(menu)

  mainWindow.on('page-title-updated', (event) => {
    event.preventDefault();
  })
  // mainWindow.once('ready-to-show', () => {
  //   mainWindow.show()
  // })

  // and load the index.html of the app.
  mainWindow.loadFile('src/page/main/index.html')

  // mainWindow.loadURL('https://web.chaoxin.com/',load_options);
  // mainWindow.loadURL('http://test.qdjl.net/phpinfo.php',load_options);
  mainWindow.show();
  // setTimeout(()=>{
  //   // mainWindow.loadURL('http://www.baidu.com');
  // },100)
  // mainWindow.webContents.openDevTools();

  // Open the DevTools.
  if (config.debug){
    globalShortcut.register('Ctrl+Z', ()=>{
      console.log('debug mode');
      // Open the DevTools.
      mainWindow.webContents.openDevTools();
      mainWindow.setKiosk(false);

    });
  }



  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();

  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
