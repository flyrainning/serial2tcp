"use strict";

class SerialCMD {
  constructor() {
    this.serial = false
    this.timeout = 1000
  }
  bind(serial){
    this.serial = serial
    this.serial.ondata(this._ondata)
  }
  setTimeout(ms){
    this.timeout=parseInt(ms) || 1000
  }
  send(cmd,callback,timeout){
    timeout = parseInt(ms) || this.timeout
    if (this.serial){
      this.serial.send(cmd)

    }
  }
  async sendasync(cmd){

  }
  _ondata(data){

  }
}
